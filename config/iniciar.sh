#usr/tandem/produccion/mwcifra
#==============================================================================#
# Nombre del Programa :  middleware.jar                                        #
# Autor               :  KS Soluciones                                         #
# Compania            :  KS Soluciones                                         #
# Fecha: 27/NOV/2013                                                           #
# Descripcion General :  Incia un proceso del middleware                       #
# Programa Dependiente:  Ninguno                                               #
# Programa Subsecuente:  Ninguno                                               #
# Cond. de ejecucion  :  Ninguna                                               #
# Dias de ejecucion   :  Se termina al iniciar el proceso deseado              #
#------------------------------------------------------------------------------#
#                              MODIFICACIONES                                  #
#------------------------------------------------------------------------------#
# Autor               :  MIGUEL ANGEL HERNANDEZ                                #
# Compania            :  KS SOLUCIONES                                         #
#------------------------------------------------------------------------------#
# Numero de Parametros: 4                                                      #
# Parametros Entrada                                                           #
#           - archivo de configuracion                                         #
#           - nombre del proceso                                               #
#           - tarjeta donde se desea levantar                                  #
#           - procesador donde se va a levantar                                #
#==============================================================================#

echo "=== CONF: $1 -  ====================================================="
echo "=== CONF: $1 -  ======   INICIA EJECUCION MIDDLEWARE   =============="
echo "=== CONF: $1 -  ====================================================="

# Variables del shell
ruta = "/usr/tandem/produccion/mwcifra"
file = "$1.properties"
objeto = "middleware-1.9.0.0.jar"
java_path = "/usr/tandem/nssjava/jdk160_h60/bin"

# Variables de entorno
export CLASSPATH=/usr/tandem1/javaexth11/lib/tdmext.jar
export _RLD_LIB_PATH=/usr/tandem1/javaexth11/lib
export PATH=/bin:/bin/unsupported:/usr/bin:/usr/ucb:/usr/local/bin:$java_path

cd $ruta

if [ -n "$1" ] ; then	
	if [ -e $ruta/configuracion/$file ] ; then
		if [ -n "$2" ]; then
			if [ -n "$3" ]; then
				if [ "$3" -eq "2" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC2
				elif [ "$3" -eq "3" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC3
				elif [ "$3" -eq "4" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC4
				elif [ "$3" -eq "5" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC5
				elif [ "$3" -eq "6" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC6
				elif [ "$3" -eq "7" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC7
				elif [ "$3" -eq "10" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC10
				elif [ "$3" -eq "11" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC11
				elif [ "$3" -eq "12" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC12
				elif [ "$3" -eq "13" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC13
				elif [ "$3" -eq "14" ]; then
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC14
				else
					add_define =TCPIP^PROCESS^NAME class=map File=\$ZTC20
				fi

				echo
                echo "=== CONF: $1 -  ====================================================="
				echo "=== CONF: $1 -  Se procede a levantar Middleware "
				echo "=== CONF: $1 -  Configuracion: $file"
                echo "=== CONF: $1 -  ====================================================="
      	
                run -cpu=$4 -name=/G/$2 java -jar ./$objeto ./configuracion/$file &
				echo
			else
				echo
	            echo "=== CONF: $1 -  ====================================================="
				echo "=== CONF: $1 -  No se establecio la tarjeta para levantar "
	            echo "=== CONF: $1 -  ====================================================="
				echo
			fi
		else
			echo
            echo "=== CONF: $1 -  ====================================================="
	        echo "=== CONF: $1 -  No se establecio el nombre del proceso "
            echo "=== CONF: $1 -  ====================================================="
			echo
		fi
	else
		echo
        echo "=== CONF: $1 -  ====================================================="
		echo "=== CONF: $1 -  No existe archivo de configuracion: $file         ==="
        echo "=== CONF: $1 -  ====================================================="
		echo
	fi
else
	echo
    echo "========================================================"
	echo "===        No indico el proceso a iniciar            ==="
    echo "========================================================"
	echo
fi
