package com.ks.pruebas;

import com.ks.Configuracion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zeroturnaround.exec.ProcessExecutor;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class App
{
    private static final Logger VMobjLog = (Logger) LogManager.getLogger(App.class);

    public static void main(String[] args)
    {
        try
        {
            VMobjLog.info("iniciando el proceso");
            if (args.length > 0)
            {
                String VLstrArchivo = Configuracion.getRutaConfiguracion() + "tiendasConsola.bd";
                File VLioArchivo = new File(VLstrArchivo);
                if (VLioArchivo.exists())
                {
                    Class.forName("org.sqlite.JDBC");
                    Connection VLobjConexion;
                    VLobjConexion = DriverManager.getConnection("jdbc:sqlite:" + VLstrArchivo);
                    Statement VLobjComando = VLobjConexion.createStatement();
                    ResultSet VLobjDatos = VLobjComando.executeQuery("SELECT * FROM tiendas");
                    if (VLobjDatos != null)
                    {
                        while (VLobjDatos.next())
                        {
                            ProcessExecutor VLobjProceso = new ProcessExecutor();

                            VLobjProceso.readOutput(true);
                            VLobjProceso.directory(new File("/usr/tandem/produccion/mwcifra"));

                            VLobjProceso.command("./iniciar.sh", VLobjDatos.getString("config").trim(), VLobjDatos.getString("proceso").replace("$", ""), VLobjDatos.getString("tarjeta"), VLobjDatos.getString("procesador"), "&");
                            String VLstrRespuesta = VLobjProceso.execute().outputUTF8();
                            VMobjLog.info(VLstrRespuesta);

                            Thread.sleep(5000);
                        }
                    }
                    else
                    {
                        VMobjLog.error("No hay datos que consultar");
                    }
                    VMobjLog.info("Se termino el proceso");
                }
                else
                {
                    VMobjLog.error("No existe el archivo de la base de datoss");
                }
            }
            else
            {
                VMobjLog.error("No se definio la ruta para trabajar");
            }
        }
        catch (Exception e)
        {
            VMobjLog.error("Problema al iniciar el programa: " + e.getMessage());
        }
    }
}
